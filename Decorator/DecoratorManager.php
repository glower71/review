<?php

namespace src\Decorator;

use src\Exceptions\DataProviderException;
use src\Integration\DataProvider;

abstract class DecoratorManager
{
    /**
     * @var DataProvider
     */
    protected $dataProvider;

    public function __construct(DataProvider $dataProvider)
    {
        $this->dataProvider = $dataProvider;
    }

    /**
     * @param array $input
     * @return array
     * @throws DataProviderException
     */
    public function get(array $input)
    {
        return $this->dataProvider->get($input);
    }
}
